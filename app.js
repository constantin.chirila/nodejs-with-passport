const express = require('express')
const expressLayouts = require('express-ejs-layouts')
const mongoose = require('mongoose')
const flash = require('connect-flash')
const session = require('express-session')
const passport = require('passport')

const app = express()

//Passport config
require('./config/passport')(passport)

//Database
const db = require('./config/keys').MongoURI
mongoose.connect(db, {useNewUrlParser: true})
    .then(() => console.log('Mongo DB connected..'))
    .catch(err => console.log(err))

// EJS layouts
app.use(expressLayouts)
app.set('view engine', 'ejs')

//Body parser
app.use(express.urlencoded({
    extended: false
}))

//Express session middleware
app.use(session({
    secret: 'test',
    resave: true,
    saveUninitialized: true
}))

//Passport middleware
app.use(passport.initialize())
app.use(passport.session())

app.use(flash())

// global vars
app.use((req, res, next) => {
    res.locals.successMessage = req.flash('success_message')
    res.locals.errorMessage = req.flash('error_message')
    res.locals.error = req.flash('error')
    next()
})

//Routes
app.use('/', require('./routes/index'))
app.use('/users', require('./routes/users'))

const PORT = process.env.PORT || 4123

app.listen(PORT, console.log(`Server started on ${PORT}`))